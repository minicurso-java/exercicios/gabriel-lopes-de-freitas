public class Smartphone extends DispositivoEletronico {
  private String modelo;

  
  public Smartphone(String marca, int anoFabricacao, String modelo) {
      super(marca, anoFabricacao);
      this.modelo = modelo;
  }

  
  public String getModelo() {
      return modelo;
  }

  public void setModelo(String modelo) {
      this.modelo = modelo;
  }
}