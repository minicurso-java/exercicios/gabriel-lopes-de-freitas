public class Tablet extends DispositivoEletronico {
  private double tamanhoTela;
    
  public Tablet(String marca, int anoFabricacao, double tamanhoTela) {
      super(marca, anoFabricacao);
      this.tamanhoTela = tamanhoTela;
  }

  public double getTamanhoTela() {
      return tamanhoTela;
  }

  public void setTamanhoTela(double tamanhoTela) {
      this.tamanhoTela = tamanhoTela;
  }
}