public class Main {
  public static void main(String[] args) {
      Smartphone smartphone = new Smartphone("Apple", 2022, "iPhone 13");
      Tablet tablet = new Tablet("Samsung", 2021, 10.5);
      Notebook notebook = new Notebook("Dell", 2020, "Intel Core i7");

      // Test the classes
      System.out.println("Smartphone: " + smartphone.getMarca() + " " + smartphone.getModelo());
      System.out.println("Tablet: " + tablet.getMarca() + " " + tablet.getTamanhoTela() + " inches");
      System.out.println("Notebook: " + notebook.getMarca() + " " + notebook.getProcessador());
  }
}