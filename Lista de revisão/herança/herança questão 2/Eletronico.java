// Subclasse Eletronico
class Eletronico extends Produto {
    private String marca;
    private String sistemaOperacional;

    public Eletronico(String nome, double preco, String marca, String sistemaOperacional) {
        super(nome, preco);
        this.marca = marca;
        this.sistemaOperacional = sistemaOperacional;
    }
}