class Livro extends Produto {
    private String autor;
    private String editora;
    private String descricao;

    public Livro(String nome, double preco, String autor, String editora, String descricao) {
        super(nome, preco);
        this.autor = autor;
        this.editora = editora;
        this.descricao = descricao;
    }
}