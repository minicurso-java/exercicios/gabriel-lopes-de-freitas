// Subclasse Roupa
class Roupa extends Produto {
    private String cor;
    private String tamanho;

    public Roupa(String nome, double preco, String cor, String tamanho) {
        super(nome, preco);
        this.cor = cor;
        this.tamanho = tamanho;
    }
}
